<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plantilla BS - UDI PHP</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <link rel="shortcut icon" href="favicon.ico">
  </head>
  <body>

    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
          <th></th>
          <th>Extra small<div class="fw-normal">&lt;576px</div></th>
          <th>Small<div class="fw-normal">≥576px</div></th>
          <th>Medium<div class="fw-normal">≥768px</div></th>
          <th>Large<div class="fw-normal">≥992px</div></th>
          <th>X-Large<div class="fw-normal">≥1200px</div></th>
          <th>XX-Large<div class="fw-normal">≥1400px</div></th>
          </tr>
        </thead>
        <tbody>
          <tr>
          <td><code>.container</code></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td>540px</td>
          <td>720px</td>
          <td>960px</td>
          <td>1140px</td>
          <td>1320px</td>
          </tr>
          <tr>
          <td><code>.container-sm</code></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td>540px</td>
          <td>720px</td>
          <td>960px</td>
          <td>1140px</td>
          <td>1320px</td>
          </tr>
          <tr>
          <td><code>.container-md</code></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td>720px</td>
          <td>960px</td>
          <td>1140px</td>
          <td>1320px</td>
          </tr>
          <tr>
          <td><code>.container-lg</code></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td>960px</td>
          <td>1140px</td>
          <td>1320px</td>
          </tr>
          <tr>
          <td><code>.container-xl</code></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td>1140px</td>
          <td>1320px</td>
          </tr>
          <tr>
          <td><code>.container-xxl</code></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td>1320px</td>
          </tr>
          <tr>
          <td><code>.container-fluid</code></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          <td><span class="text-body-secondary">100%</span></td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="container bg-primary text-center">
      <p>Clase container</p>
    </div>

    <div class="container-sm bg-primary text-center">
      <p>Clase container-sm</p>
    </div>

    <div class="container-md bg-primary text-center">
      <div class="row">
        <div class="col bg-alert">
          <p>Columna md</p>
        </div>
        <div class="col bg-success">
          <p>Columna md</p>
        </div>
        <div class="col bg-secondary">
          <p>Columa md</p>
        </div>



      </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>
