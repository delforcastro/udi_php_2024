<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
      <h2>Soy Inicio</h2>
      <h3>
          <?php
              $usuario = '    Juan Perez    ';
              $Usuario = 'María';
              $USUARIO = 'Jorge';
              //IMPORTANTE: Las variables son case sensitive!
              //echo $usuario;

              echo strlen($usuario); //contar cuántos caracteres tiene un str

              echo '</br>';

              echo strtolower($usuario) . '</br>'; //pasa a minúscula
              echo strtoupper($usuario) . '</br>'; //pasa a mayúscula
              echo substr($usuario, 3, 4) . '</br>'; //extrae una subcadena
              echo trim($usuario) . '</br>'; //extrae los espacios en blanco antes y
                                              //después del primer y último caracter visible
                                              //respectivamente

              $nombre = 'Ana';
              $apellido = 'Fernandez';
              //FERNANDEZ, Ana
              echo trim(strtoupper($apellido)) . ', ' . trim($nombre) ;
              echo '</br>';

              $nombre = trim($nombre);
              $apellido = trim(strtoupper($apellido));

              echo "$apellido, $nombre";
              















          ?>
      </h3>

  </body>
</html>
